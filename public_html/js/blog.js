$(function () {
    var APPLICATION_ID = "E760745D-8DF1-EE4F-FFF7-D7E5E543CD00",
        SECRET_KEY = "CA33F55C-F33B-41F8-FF5B-61F0AB764D00",
        VERSION = "v1";
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
 
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYY");
    });
 
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
 
});

function Posts(args) {
    args = args || {};
    this. title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}
